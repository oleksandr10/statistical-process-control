# Statistical Process Control
---

[[_TOC_]]


This project aims to simulate a multivariate production process, and how different parameters affect the detection rate of the out-of-control process when **$`\mu`$** if shifted in one of the variables.
It it of interest to see how correlated variables might affect the detection rate of the out-of-control batches.
The method used to create the control chart is Hotelling $`T^2`$, which is suitable for a multivariate problem when only want control chart is wanted.


## Model
The model can be formulated as:

```math
Y_{ij} = \mu + \tau_i + \epsilon_{ij}
```
$`\tau_i`$ component corresponds to i'th batch and $`\epsilon_{ij}`$ to the random noise associated with the j'th observation in batch i. 


## Data generation
Data generating functon can be found in [[data_gen.R]] file. 
It generates data with 3 variables and $`n`$ groups with 20 observations per group.
The correlation coeficient of the 2 first variables can be pre-specified, together with the variance componenet due to groups and the $`\mu`$ of the process.  

## Dashboard
Dashboard is a visualization componenet of the simulation. 

The following parameters can be changed in real time:
$`\mu`$ shift 
variation between batches
correlation coeficient between the first 2 variables
whether the shifted variable is one of the correlated ones or the uncorrelated one. 

"New sample" button randomly generates a new sample. 

